import { StatusBar } from "expo-status-bar";
import React, { useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TextInput,
  textforgot,
  Button,
  TouchableOpacity,
} from "react-native";
 
export default function App() {
  const [Nama, setnama] = useState("");
  const [kelas, setkelas] = useState("");
  const [programstudi, setprogramstudi] = useState("");

  return (
    <View style={styles.container}>
      <StatusBar style="auto" />
      <Image style={styles.image} source={require('./faiz.jpg')} />
      <Text style={styles.text}>Welcome Muhamad faiz aulia
      </Text>
      <Text style={styles.text2}>My biodata
      </Text>
      <Text style={styles.text3}>Name :
      </Text>
      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Muhamad faiz aulia"
          placeholderTextColor="#000"
          onChangeText={(nama) => setnama(nama)}
        />
      </View>
     <Text style={styles.text4}>Kelas :
      </Text>
      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Malam C"
          placeholderTextColor="#000"
          secureTextEntry={true}
          onChangeText={(kelas) => setkelas(kelas)}
        />
      </View>
      <Text style={styles.text5}>Program Studi :
      </Text>
      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="Teknik Informatika"
          placeholderTextColor="#000"
          secureTextEntry={true}
          onChangeText={(programstudi) => setprogramstudi(programsutdi)}
        />
      </View>
      <Text style={styles.text5}>Akun Sosmed :
      </Text>
      <View style={styles.inputView}>
        <TextInput
          style={styles.TextInput}
          placeholder="@muhamadfaiz.a"
          placeholderTextColor="#000"
          secureTextEntry={true}
          onChangeText={(programstudi) => setprogramstudi(programsutdi)}
        />
      </View>
    </View>
  );
}
 
const styles = StyleSheet.create({
  container: {
    flex: 100,
    backgroundColor: "#70D0C3",
    alignItems: "center",
    justifyContent: "center",
  },
  
  text: {
    marginTop: 20,
    textAlign : "left",
    fontWeight : "bold",
    color : "00000",
    marginBottom: 0,
    
      },

 text2: {
    textAlign : "left",
    fontWeight : "normal",
    color : "00000",
    marginBottom: 20,
    
      },
  text3: {
    justifyContent: "center",
    alignItems: "left",
    fontWeight : "bold",
    color : "00000",
    marginBottom: 0,
      },
    text4: {
    justifyContent: "center",
    alignItems: "left",
    fontWeight : "bold",
    color : "00000",
    marginBottom: 0,
      },
 text5: {
    justifyContent: "center",
    alignItems: "left",
    fontWeight : "bold",
    color : "00000",
    marginBottom: 0,
      },
  image: {
    borderRadius: 100,
    width: 100,
    height: 100,
    left: 0,
    marginBottom: 0,
  },
 
  inputView: {
    backgroundColor: "#fff",
    borderRadius: 25,
    width: "70%",
    height: 45,
    marginBottom: 20,
    alignItems: "left",
  },
 
  TextInput: {
    height: 50,
    flex: 1,
    padding: 10,
    marginLeft: 5,
  },
 
});